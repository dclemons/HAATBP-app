
/*
    Detect device
*/

var $device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

 if ($device){
    $("html").addClass("device")
 }else {
    $("html").addClass("desktop")
 }

/*
    config firebase
*/
// var config = {
//     apiKey: "AIzaSyAjswMYQ0PwMenYUUscg9kwid0cfoTARDI",
//     authDomain: "here-are-all-the-black-people.firebaseapp.com",
//     databaseURL: "https://here-are-all-the-black-people.firebaseio.com/",
//     projectId: "here-are-all-the-black-people",
//     storageBucket: "",
//     messagingSenderId: "454579606277"
// };

 var config = {
    apiKey: "AIzaSyAiwWOm88CcLK5p5iYNx9k4ulRndChq3z4",
    authDomain: "haatbp-production.firebaseapp.com",
    databaseURL: "https://haatbp-production.firebaseio.com",
    projectId: "haatbp-production",
    storageBucket: "haatbp-production.appspot.com",
    messagingSenderId: "672403645254"
  };

firebase.initializeApp(config);
var db = firebase.database();

/*
    config amazon s3
*/
// var albumBucketName = 'd5-haatbptwo';
// var bucketRegion = 'us-east-1';
// var IdentityPoolId = 'us-east-1:db6b30a8-f7a6-481f-8f29-bd3cc584db8b';
// var _canvas;
var albumBucketName = 'haatbp';
var bucketRegion = 'us-east-2';
var IdentityPoolId = 'us-east-2:a3bcb2f6-50a4-4bbe-9916-af4c9a7a7fbe';
var _canvas;

AWS.config.update({
    region: bucketRegion,
    credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IdentityPoolId
    })
});

var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: albumBucketName}
});

/*
    helper
*/
function dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for(var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}

function rotateAndPaintText( context, text, angleInRad , positionX, positionY, axisX, axisY ) {
    context.translate( positionX, positionY );
    context.rotate( angleInRad );
    context.fillText( text, -axisX, -axisY );
    context.rotate( -angleInRad );
    context.translate( -positionX, -positionY );
}

function rotateAndPaintImage( context, image, angleInRad , positionX, positionY, axisX, axisY ) {
    context.translate( positionX, positionY );
    context.rotate( angleInRad );
    context.drawImage( image, -axisX, -axisY, 278, 371 );
    context.rotate( -angleInRad );
    context.translate( -positionX, -positionY );
}


function buildImage() {
    _canvas = document.createElement('canvas');
    _canvas.id = "finalcanvasdrawing";
    var _ctx = _canvas.getContext('2d');
    _canvas.width  = 612;
    _canvas.height = 612;
    //document.body.appendChild(_canvas); // adds the canvas to the body element
    //document.getElementsByTagName('body')[0].appendChild(_canvas);

    var _mask = new Image();
    _mask.onload = function() {
        $(".tradingcard img")[0].setAttribute('crossOrigin', 'anonymous');
        rotateAndPaintImage( _ctx, $(".tradingcard img")[0], -2.5*TO_RADIANS, 30, 60, 0, 0);
        _ctx.drawImage(_mask, 0, 0, 612, 612 );
        _ctx.textAlign = 'left';
        _ctx.font = '700 9px Rubik';                           
        _ctx.fillStyle = '#FFFFFF';
        rotateAndPaintText( _ctx, new_submission.role.toUpperCase(),  3.5*TO_RADIANS, 305, 120, 0, 0 );
        _ctx.font = '700 18px Rubik';                           
        _ctx.fillStyle = '#000000';
        rotateAndPaintText( _ctx, new_submission.firstname.toUpperCase(),  3.5*TO_RADIANS, 303, 142, 0, 0 );
        rotateAndPaintText( _ctx, new_submission.lastname.toUpperCase(),  3.5*TO_RADIANS, 301, 165, 0, 0 );
        _ctx.font = '700 12px Rubik';                           
        _ctx.fillStyle = '#000000';
        rotateAndPaintText( _ctx, new_submission.education.toUpperCase(), 3.5*TO_RADIANS, 300, 227, 0, 0 );
        rotateAndPaintText( _ctx, new_submission.city.toUpperCase(), 3.5*TO_RADIANS, 296, 276, 0, 0 );
        rotateAndPaintText( _ctx, new_submission.email.toUpperCase(), 3.5*TO_RADIANS, 293, 325, 0, 0 );
        rotateAndPaintText( _ctx, new_submission.social.toUpperCase(), 3.5*TO_RADIANS, 290, 375, 0, 0 );
        rotateAndPaintText( _ctx, new_submission.spiritanimal.toUpperCase(), 3.5*TO_RADIANS, 286, 424, 0, 0 );

        var _dataUrl = _canvas.toDataURL("image/png", .8);
        var _blobData = dataURItoBlob(_dataUrl);

        new_submission.carduuid = makeuuid();
        //https://d5-haatbptwo.s3.amazonaws.com/
        new_submission.card = "https://s3.us-east-2.amazonaws.com/haatbp/"+new_submission.carduuid+".png";
        s3.upload({
            Key: new_submission.carduuid + ".png",
            Body: _blobData,
            ACL: 'public-read'
        }, function(err, data) {
            if (err) {
                showError();
                return console.log('There was an error uploading your photo: ', err.message);
            }
            console.log('Successfully uploaded photo.');
        });

    }
    _mask.src = "./img/trading_card_mask_"+new_submission.department.toLowerCase()+"_5.png";
}



/*
    camera functions
*/
function takePicture() {

    if ($device){
       canvas.getContext('2d').drawImage(video, 0, 0, 480, 640, 0, 0, 480, 640);
    } else {

      var ctx = canvas.getContext('2d');
      ctx.save(); 
      ctx.translate(672, 0);
      ctx.scale(-1, 1);
      ctx.drawImage(video, 0, 0, 896, 672 );
      ctx.restore(); 

    }

    $("#cameraview .toplevelinfo").addClass("on");

    $("#canvas").show();
    $("#cameraview .buttons .keep, #cameraview .buttons .restart").show();
    setTimeout(function() {
        $("#video").hide();
    }, 100);

    stream.getTracks()[0].stop();
    console.log("turning off camera");
    
    var dataUrl = canvas.toDataURL("image/jpeg", .8);
    // console.log("dataUrl : "+dataUrl);
    var blobData = dataURItoBlob(dataUrl);
    // console.log("blobdata : ");
    // console.dir(blobData);

    new_submission.uuid = makeuuid();
    // console.log("new_submission.uuid "+new_submission.uuid);
    //https://d5-haatbptwo.s3.amazonaws.com/
    new_submission.photo = "https://s3.us-east-2.amazonaws.com/haatbp/"+new_submission.uuid+".jpeg";
    s3.upload({
        Key: new_submission.uuid + ".jpeg",
        Body: blobData,
        ACL: 'public-read'
    }, function(err, data) {
        if (err) {
            showError();
            return console.log('There was an error uploading your photo: ', err.message);
            //PUT IN AN ERROR MESSAGE HERE
        }
        console.log('Successfully uploaded photo.');
    });
}

function toggleFullScreen() {
    var el = document.documentElement;
    var rfs = el.webkitRequestFullScreen;

    rfs.call(el);
}

function exitHandler() {
    if (document.webkitIsFullScreen) {
        bFullscreen = false;
    }
}

function startCountdown() {
    clearTimeout(camera_t);
    timer--;
    if( timer <= 0 ) {
        $("#countdown").hide();
        setTimeout(takePicture, 100);
    } else {
        $("#countdown h2").text( timer );
        camera_t = setTimeout(startCountdown, 1000);
    }
}


/*
    views
*/
function showThankYou() {
    setTimeout(function(){
        console.log( new_submission.card );
        $.post('/sendemail/', new_submission, function(resp) {
            console.log( resp );
            if( resp && resp.msg && resp.msg == "success" ) {
                $("#logo").hide();
                $("#thankyou").css({opacity: 1}).show();
                $("#thankyou").delay(2000).animate({
                    opacity: 0
                }, 3000, function() {
                    $("#thankyou").hide();
                    $("body").removeClass();
                    setTimeout(function(){
                        showCamera();
                    }, 2000);
                });
            }
    }, "json");
    }, 3000);
}

function hideFinalCard() {
    $("body").removeClass().addClass("last");
    $("#finalcard").hide();
    $("body").removeClass("white")
}

function showFinalCard() {
    var bust = Math.floor(Math.random()*1000);

    $(".loader").show();

    console.log(new_submission.photo);
    $("#finalcard").addClass( new_submission.department );
    $("#finalcard .tradingcard-wrapper .tradingcard .photo").attr("src", new_submission.photo);
    // $("#finalcard .tradingcard-wrapper .tradingcard .mask").addClass(new_submission.department.toLowerCase());
    $("#finalcard .tradingcard-wrapper .tradingcard .role").text(new_submission.role);
    setTimeout(function() {
        $("#finalcard .tradingcard-wrapper .tradingcard .firstname").text(new_submission.firstname);
        $("#finalcard .tradingcard-wrapper .tradingcard .lastname").text(new_submission.lastname);

    }, 500);
    setTimeout(function() {
        $("#finalcard").show();
        $("#finalcard").animate({
                opacity: 1
        }, 1000);
        $(".loader").hide();
    }, 7000);
   // setTimeout(buildImage, 500);
    setTimeout(function(){
        buildImage();
    }, 3000);
    $("body").addClass("white")
}

function hideEnterContact() {
    $("#entercontact").hide();
}

function showEnterContact() {
    $("#entercontact").show();
}

function hideDepartment() {
    $("#choosedepartment").hide();
}

function showDepartment() {
    $("#choosedepartment").show();
    $("#choosedepartment .toplevelinfo").addClass("on");
}

function hideRoles() {
    $("#chooserole").hide();
    $("#chooserole .toplevelinfo").removeClass("on");    
}

function showRoles() {
    $("#chooserole").show();
    $("#chooserole .toplevelinfo").addClass("on");
    setColor();
}

function showCamera() {
    $("body").removeClass().addClass("grey");
    new_submission = {};
    $("#cameraview").show();
    $("#logo").show();
    $("#entercontact form input.field").each(function(index, value) {
        $(this).val("").text("");
    });
    //social needs a special selector to clear
    $("#entercontact form input.social").val("").text("");
     //reset radio to url default
    $("input[value='url']")[0].checked = true;
    //clear all checkboxes   
    $("input[type='checkbox']:checked").each(function(index, value) {
        $(this)[0].checked = false;
    })    
    $("#chooserole.other input").val("").text("");

    console.log("turning on camera");
    navigator.getMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    navigator.getMedia({
        video: true,
        audio: false
    }, function(_stream) {
        stream = _stream;
        if (navigator.mozGetUserMedia) {
            video.mozSrcObject = _stream;
        } else {
            var vendorURL = window.URL || window.webkitURL;
            video.src = vendorURL ? vendorURL.createObjectURL(_stream) : _stream;
        }
        video.play();
    }, function(err) {
        console.log("An error occured! ", err);
    });


    timer = 3;
    $("#countdown h2").text( timer );

    $("#canvas").hide();
    $("#video").show();

    $("#cameraview .toplevelinfo").removeClass("on");

    $("#canvas").hide();
    $("#cameraview .buttons").show();
    $("#cameraview .buttons .takephoto").show();
    $("#cameraview .buttons .keep, #cameraview .buttons .restart").hide();
}

function hideCamera() {
    $("#camerabuttons").hide();
    $("#cameraview").hide();    
}

function showRetake(){
    $("#retake").show();
    $("#retake-overlay").show();
}

function hideRetake(){
    $("#retake").hide();
    $("#retake-overlay").hide();
}

function showError(){
    $("#error-photo").show();
    $("#retake-overlay").show();
}

function hideError(){
    $("#error-photo").hide();
    $("#retake-overlay").hide();
}

function setColor(){
    var otherColor = "A4478C";
    var creativeColor = "6F470C";
    var prColor = "226A5E";
    var productionColor = "920F26"; 
    var accountColor = "087D95";
    var strategyColor= "8B2015"; 
    var color;

    if ( $("body").hasClass("creative") ) {
         color = creativeColor;
    } else if ( $("body").hasClass("pr") ){
         color = prColor;
    } else if ( $("body").hasClass("production") ){
         color = productionColor;
    } else if ( $("body").hasClass("strategy") ){
         color = strategyColor;
    } else if ( $("body").hasClass("account") ){
         color = accountColor;
    } else if ( $("body").hasClass("other") ){
         color = otherColor;
    } 

    $("#chooserole .description").css("color","#"+color);
    $("#chooserole .buttons .button.back").css("color","#"+color);
    $("#chooserole .buttons .button.next").css("color","#"+color);
    $("ul li input.checkbox").css("color","#"+color);
    $(".creative #entercontact .buttons .button.next").css("color", "#"+color);
    $("#entercontact .buttons .button.next.off").css({"color":"#"+ color, "border":"3px solid #"+color});
    $(".release-input p").css("color", "#"+color);
}

var video, canvas;

//4:3 
if ($device) { 
    var width = 672;
    var height = 896;
} else {
    var width = 896;
    var height= 672;
}
// var width = 672, height = 896;
var streaming = false;
var bFullscreen = false;
var camera_t, timer;
var stream;
var new_submission = {}
var submission_id;
var TO_RADIANS = Math.PI/180;
$(document).ready(function() {


    video = $("#video")[0];
    canvas = $("#canvas")[0];

    video.addEventListener('canplay', function(ev) {
        if (!streaming) {
            video.width = width;
            video.height = height;
            canvas.width = 480;
            canvas.height = 640;
            streaming = true;
        }
    }, false);

    document.addEventListener("keydown", function(e) {
        if (e.keyCode == 13 && !bFullscreen) {
            bFullscreen = true;
            toggleFullScreen();
        }
    }, false);
    document.addEventListener('fullscreenchange', exitHandler, false);

    db.ref("submission_id").on('value', function(snapshot) {
        submission_id = snapshot.val() || 0;
        submission_id++;
        new_submission.sid = submission_id;
        console.log( "hello, current sid: " + submission_id );
    });

    /*
        final card
    */
    $("#finalcard .buttons .done").click(function(){
        hideFinalCard();
        db.ref('submission_'+submission_id).set(new_submission, function() {
            console.log("data set for submission_"+submission_id+", data: ");
            console.log(new_submission);
            db.ref("submission_id").set(submission_id, function() {
                console.log("updated submission_id to: " + submission_id);
                showThankYou();
            });
        });
    });

    /*
        enter contact info
    */

    $("#entercontact input").on('change keydown paste input', function(){

        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;

        if (    $("#entercontact form input.firstname").val() != "" &&
                $("#entercontact form input.lastname").val() != "" &&
                $("#entercontact form input.education").val() != "" &&
                $("#entercontact form input.city").val() != "" &&
                $("#entercontact form input.email").val() != "" &&
                $("#entercontact form input.social").val() != "" &&
                $("#entercontact form input.spiritanimal").val() != "" &&
                $('.form-release .checkbox')[0].checked != false &&
                re.test($("#entercontact form input.email").val()) != false
                )
        {
            $("#entercontact .buttons .next").removeClass("off");
            $("#entercontact .buttons .next").addClass("on");
            $('.checkbox').val(true);
        } else {
            $("#entercontact .buttons .next").addClass("off");
            $("#entercontact .buttons .next").removeClass("on");
            $('.checkbox').val(false);
        }
    });

    // Press enter when not all fiedls or complete or email isnt
    $('body').on('click',"#entercontact .buttons .next.off",function() {

        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
            
        if (re.test($("#entercontact form input.email").val()) == false) {
            $("#entercontact form .form-email p.error-state").css("display","block");
            console.log("bad");
            console.log("bad " + $("#entercontact form input.email").val() + " " + re.test($("#entercontact form input.email").val()) );
        }  else {
            $("#entercontact form .form-email p.error-state").css("display","none");
            console.log("good");
            console.log("good " + $("#entercontact form input.email").val() + " " + re.test($("#entercontact form input.email").val()) );
        }
        if ($("#entercontact form input.firstname").val() == "" ) {
            $("#entercontact form .form-first-name p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-first-name p.error-state").css("display","none");
        }
        if ($("#entercontact form input.lastname").val() == "") {
            $("#entercontact form .form-last-name p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-last-name p.error-state").css("display","none");
        }
        if ($("#entercontact form input.education").val() == "") {
            $("#entercontact form .form-education p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-education p.error-state").css("display","none");
        }
        if ($("#entercontact form input.city").val() == "") {
            $("#entercontact form .form-city p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-city p.error-state").css("display","none");
        }
        if ($("#entercontact form input.social").val() == "") {
            $("#entercontact form .form-site p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-site p.error-state").css("display","none");
        }
        if ($("#entercontact form input.spiritanimal").val() == "") {
            $("#entercontact form .form-spirit-animal p.error-state").css("display","block");
        }  else {
            $("#entercontact form .form-spirit-animal p.error-state").css("display","none");
        } 
        // Linkedin not required
        // if ($('.form-linkedin .checkbox')[0].checked == false) {
        //     $("#entercontact form .form-linkedin p.error-state").css("display","block");
        // } else {
        //     $("#entercontact form .form-linkedin p.error-state").css("display","none");
        // }

        if ($('.form-release .checkbox')[0].checked == false) {
            $("#entercontact form .form-release p.error-state").css("display","block");
        } else {
            $("#entercontact form .form-release p.error-state").css("display","none");
        }
    });

    //On click radio button check which is checked and add the correct starting text
    $('body').on('click',"#entercontact .choice input",function() {
         if ($('.choice.url input')[0].checked != false) {
            $("input.social").attr("placeholder", "jcitizen.com");
            $("#entercontact .form-site").attr("data-attr","url");
            $(".pre-placeholder").text("www.");
            $("#entercontact ul li.form-site input.social").css("margin-left","59px");
         } else if ($('.choice.twitter input')[0].checked != false) {
            $("input.social").attr("placeholder", "jcitizen");
            $("#entercontact .form-site").attr("data-attr","twitter");
            $(".pre-placeholder").text("@");
            $("#entercontact ul li.form-site input.social").css("margin-left","19px");
         } else if ($('.choice.instagram input')[0].checked != false) {
            $("input.social").attr("placeholder", "jcitizen");
            $("#entercontact .form-site").attr("data-attr","instagram");
            $(".pre-placeholder").text("@");
            $("#entercontact ul li.form-site input.social").css("margin-left","19px");
         }
    });


    // $("#entercontact .buttons .next").click(function() {
    $('body').on('click',"#entercontact .buttons .next.on",function() {


        $("#entercontact form input.field").each(function(index, value) {
            new_submission[$(this).attr("name")] = $(this).val();
            console.log("the values " + $(this).attr("name") + " " + $(this).val())
        });


        //we want to submit website/social field seperatately to apply some logic
        var social;

        if ( $("input[type='radio']:checked").val() == "url" ) {
           social = "www." + $("#entercontact form input.social").val();

        } else if ( $("input[type='radio']:checked").val() == "twitter" ) {
           social = "@" + $("#entercontact form input.social").val();

        } else if ( $("input[type='radio']:checked").val() == "instagram" ) {

           social = "@" + $("#entercontact form input.social").val();
        }

        console.log($("#entercontact form input.social").attr("name"));
        console.log(social);

        new_submission[$("#entercontact form input.social").attr("name")] = social;
        new_submission[$("input[type='radio']:checked").attr("name")] = $("input[type='radio']:checked").val();

        $("#entercontact form input[type='radio']:checked")[0].checked = false;

        hideEnterContact();
        showFinalCard();
        $("#entercontact form p.error-state").css("display","none");
    });

    $("#entercontact .buttons .back").click(function() {
        hideEnterContact();
        showRoles();
    });

    /*
        choose role
    */
    $("#chooserole .buttons .back").click(function(){
        hideRoles();
        $("body").removeClass().addClass("grey");
        showDepartment();
    });

    $("#chooserole .roles.production li, #chooserole .roles.creative li").click(function() {
        new_submission.role = $(this).text();

        hideRoles();
        showEnterContact();
    });

    $("#chooserole .buttons .next").click(function() {
        if( $("#chooserole").hasClass("other") ) {
            new_submission.role = $("#chooserole.other input").val();
        }

        hideRoles();
        showEnterContact();
    });

    /*
        choose department
    */
    $("#choosedepartment ul li").click(function() {
        new_submission.department = $(this).data("dept");
        
        var description = $(this).data("description");
        $("#chooserole .description").text(description);
        
        var role = $(this).attr("class");
        $("#chooserole").removeClass().addClass(role);
        $("body").removeClass().addClass(role);
        
        new_submission.role = $(this).data("role");

        hideDepartment();
        showRoles();
    });

    /*
        camera buttons
    */
    $("#cameraview .buttons .takephoto").click(function() {
        $("#countdown").show();
        timer = 3;
        $("#cameraview .buttons .takephoto").hide();
        camera_t = setTimeout(startCountdown, 1200);
    });

    $("#cameraview .buttons .keep").click(function() {
        hideCamera();
        showDepartment();
    });

    $("#cameraview .buttons .restart").click(function(){
        showRetake();
    });

    $("#retake .buttons .back").click(function(){
        hideRetake();
        showCamera();
    });

    $("#retake .buttons .forward").click(function(){
        hideRetake();
        hideCamera();
        showDepartment();
    });
    

    setTimeout(function() {
        $("#logo").addClass("small");
        setTimeout(function(){
            $("#cameraview").addClass("on");
            $("body").removeClass().addClass("grey");
        }, 750);
    }, 1000);

    $("#cameraview")[0].addEventListener('webkitTransitionEnd', function(event) { 
        if( !streaming ) {
            showCamera();
        }
     }, false);


    $("#error-photo .buttons .back").click(function(){
        hideError();
    });
});




